package com.ventia.pulse;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

@Component
public class ProcessInput {
	
	@NotNull 
	private String jobNumber;
	
	@NotNull 
	private String contract;
	
	@NotNull 
	private String jobDescription;
	
	@NotNull 
	private String location;
		

	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}
	
	public String getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}
	
	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	
}