package com.ventia.pulse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Component
@RestController
public class Services {
	
	@Autowired
	private RuntimeService runtimeService;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@RequestMapping(value = "/start-my-process", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void startMyProcess(@RequestBody @Validated ProcessInput input) {
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("data_contract", input.getContract());
        variables.put("data_location", input.getLocation());
        variables.put("data_description", input.getJobDescription());
        variables.put("data_jobNumber", input.getJobNumber());
		runtimeService.startProcessInstanceByKey("job",variables);
	}
	
	@RequestMapping(value = "/sqlLookup", method = RequestMethod.POST)
	    public List<Map<String, Object>> sql(@RequestBody String sql){

	        return jdbcTemplate.queryForList(sql);		        
	}
	
	@RequestMapping(value = "/businessRuleLookup", method = RequestMethod.POST)
    public List<Map<String, Object>> businessRuleLookup(String rulename, String contract){			
        return jdbcTemplate.queryForList("select * from business_rule where rule_name = '" + rulename + "' and contract = '" +  contract + "'");		        
	}
	
}