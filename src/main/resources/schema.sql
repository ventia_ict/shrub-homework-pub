create table public.jobs (job_id varchar(100), job_description varchar(100), job_status integer, job_location varchar(100));

create table public.business_rule (rule_name varchar(100), contract varchar(50), output varchar(100));

create table public.locations (location varchar(100), contract varchar(50), priority varchar(10));