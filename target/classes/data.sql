INSERT INTO jobs (job_id, job_description, job_status, job_location) VALUES
  ('JOB1', 'Parliament House', 1,'A001'),
  ('JOB2', 'Harbor Bridge',1,'A002'),
  ('JOB3', 'Big Ben',1,'A003'),
  ('JOB4', 'MCG',1,'A004'),
  ('JOB5', 'SCG',2,'A005');

  
INSERT INTO business_rule (rule_name, contract, output) VALUES
  	('requires_travel_state','ACME','true'),
  	('requires_travel_state','NOT ACME','false'),
  	('requires_gloves','ACME','true'),
  	('requires_gloves','NOT ACME','false');

  
INSERT INTO locations (location, contract, priority) VALUES
  	('A001','ACME','1'),
  	('A002','NOT ACME','2');